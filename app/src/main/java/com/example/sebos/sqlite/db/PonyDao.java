package com.example.sebos.sqlite.db;

import android.content.ContentValues;
import com.example.sebos.sqlite.Pony;

import java.util.List;

/**
 * Created by sebos on 28.11.2017.
 */

public interface PonyDao {

    PonyAdapter open();
    void close();
    void deleteTable();
    void insert(Pony pony);
    long count() ;
    boolean isEmpty();
    List<Pony> getPonysHigherThan(double height);
    int wrapSameHeightToLowest();
    int updatePony(Pony pony);
    void killAll();
    void killOnePony(int id);
}
