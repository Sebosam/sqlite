package com.example.sebos.sqlite.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.example.sebos.sqlite.Pony;

import java.util.ArrayList;
import java.util.List;

import static com.example.sebos.sqlite.db.CreatureContract.PonyEntry.*;

/**
 *
 * Created by sebos on 26.11.2017.
 */

public class PonyAdapter implements PonyDao{

    private final Context mContext;
    private SQLiteDatabase mDatabaseWriteable;
    private SQLiteDatabase mDatabaseReadable;
    private DataBaseHelper mDbHelper;

    public PonyAdapter(Context context) {
        this.mContext = context;
    }

    public PonyAdapter open() throws SQLException {
        mDbHelper = new DataBaseHelper(mContext);
        mDatabaseWriteable = mDbHelper.getWritableDatabase();
        mDatabaseReadable = mDbHelper.getReadableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public void deleteTable(){
        mDatabaseWriteable.execSQL("drop table if exists " + TABLE_NAME + ';');
    }

    public void insert(Pony pony){
//
        /***** 1 ****/
        String insertQuery = "INSERT INTO " + TABLE_NAME + " ("
                + COLUMN_NAME_NAME + ", " + COLUMN_NAME_HEIGHT + ", "
                + COLUMN_NAME_DESCRIPTION + ", " + COLUMN_NAME_IMAGE +")" +
                " Values ('" + pony.getName() + "', '" + pony.getHeight() + "', '" + pony.getDescription() + "', '" + pony.getImageId() + "')";
        mDatabaseWriteable.execSQL(insertQuery);

        ///***** 2 ****/
        //ContentValues values = createContentValues(pony);
        //mDatabaseWriteable.insert(TABLE_NAME, null, values);
        //try{
        //
        //} catch (SQLiteConstraintException exception){
        //    Log.d("SqlException","Added previously");
        //}
    }

    public long count() {

        /***** 1 ****/
        //String query = "SELECT count(*) FROM " + TABLE_NAME;
        //Cursor rawCursor = mDatabaseReadable.rawQuery(query, null);
        //rawCursor.moveToFirst();
        //long count =  rawCursor.getInt(0);
        //rawCursor.close();
        //return count;

        /****** 2 ****/
        //Cursor cursor = mDatabaseReadable.query(TABLE_NAME, null, null, null, null, null, null);
        //long count = cursor.getCount();
        //cursor.close();
        //return count;

        /****** 3  API > 11****/
        return DatabaseUtils.queryNumEntries(mDatabaseReadable, TABLE_NAME);
    }

    public boolean isEmpty() {
        return count() == 0;
    }

    private ContentValues createContentValues(Pony pony){
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_NAME, pony.getName());
        values.put(COLUMN_NAME_HEIGHT, pony.getHeight());
        values.put(COLUMN_NAME_DESCRIPTION, pony.getDescription());
        values.put(COLUMN_NAME_IMAGE, pony.getImageId());
        return values;
    }

    public List<Pony> getPonysHigherThan(double height) {

        // 1. build the query
        //String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME_HEIGHT + " >= ?";

        //Cursor rawCursor = mDatabaseReadable.rawQuery(query, new String[] {String.valueOf(height)});

        Cursor cursor = mDatabaseReadable.query(TABLE_NAME, // a. table
                null, // b. column names
                COLUMN_NAME_HEIGHT + " >= ? ", // c. selections
                new String[] {String.valueOf(height)}, // d. selections args
                null, // e. group by
                null, // f. having
                null, // g. order by
                null); // h. limit)



        List<Pony> ponys = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do{
                Pony pony = new Pony(cursor.getString(1), cursor.getDouble(2),cursor.getString(3), cursor.getString(4));
                pony.setId(cursor.getInt(0));
                ponys.add(pony);
            }while(cursor.moveToNext());
        }
        cursor.close();
        return ponys;
    }

    public int wrapSameHeightToLowest() {
        String updateQuery ="UPDATE " + TABLE_NAME + " SET " + COLUMN_NAME_HEIGHT + " = 6 WHERE " + COLUMN_NAME_HEIGHT + " < 6";
        Cursor rawCursor = mDatabaseWriteable.rawQuery(updateQuery, null);

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_HEIGHT, 6);

        return mDatabaseWriteable.update(TABLE_NAME, //table
                values, // column/value
                COLUMN_NAME_HEIGHT + " < 6",
                null);
    }

    public int updatePony(Pony pony){
        ContentValues values = createContentValues(pony);

        return mDatabaseWriteable.update(TABLE_NAME, //table
                values, // column/value
                _ID + " = " + pony.getId(),
                null);
    }

    public void killAll() {
        mDatabaseWriteable.delete(TABLE_NAME,
                null,
                null);
    }

    public void killOnePony(int id) {
        mDatabaseWriteable.delete(TABLE_NAME,
                _ID + " = ?",
                new String[] {String.valueOf(id)});
    }
}
