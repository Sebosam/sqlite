package com.example.sebos.sqlite.Utils;

/**
 * Created by sebos on 27.11.2017.
 */

public interface DeleteCallback {

    void onDeleteItem(int id);

}
