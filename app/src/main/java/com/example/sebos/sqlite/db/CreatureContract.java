package com.example.sebos.sqlite.db;

import android.provider.BaseColumns;

/**
 * Created by sebos on 26.11.2017.
 */

public class CreatureContract {

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + PonyEntry.TABLE_NAME + "(" +
                    PonyEntry._ID + " INTEGER PRIMARY KEY," +
                    PonyEntry.COLUMN_NAME_NAME + " TEXT," +
                    PonyEntry.COLUMN_NAME_HEIGHT + " REAL," +
                    PonyEntry.COLUMN_NAME_DESCRIPTION + " TEXT," +
                    PonyEntry.COLUMN_NAME_IMAGE + " TEXT)";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXIST " + PonyEntry.TABLE_NAME;

    public static class PonyEntry implements BaseColumns {
        public static final String TABLE_NAME = "pony";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_HEIGHT = "height";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_IMAGE = "image";
    }
}
