package com.example.sebos.sqlite.Utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sebos.sqlite.Pony;
import com.example.sebos.sqlite.R;

import java.util.List;

/**
 * Created by sebos on 27.11.2017.
 */

public class CustomArrayAdapter extends ArrayAdapter<Pony> {

    private DeleteCallback delegate = null;
    private CustomArrayAdapter mArrayAdapter = this;

    public CustomArrayAdapter(Context context, List<Pony> listHolder, DeleteCallback delegate) {
        super(context, R.layout.list_item, listHolder);
        this.delegate = delegate;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        final Pony item = getItem(position);
        View rowView = convertView;
        if (convertView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
            viewHolder = new ViewHolder(rowView);
            rowView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) rowView.getTag();
        }

        viewHolder.name.setText(item.getName());
        viewHolder.height.setText(String.valueOf(item.getHeight()));
        viewHolder.description.setText(item.getDescription());
        viewHolder.image.setImageDrawable(getImageDrawableFromId(item.getImageId()));

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mArrayAdapter.remove(item);
                mArrayAdapter.notifyDataSetChanged();
                delegate.onDeleteItem(item.getId());
            }
        });

        return rowView;
    }

    public static class ViewHolder {

        private TextView name;
        private TextView height;
        private TextView description;
        private ImageView image;
        private Button delete;

        public ViewHolder(View view) {
            name = view.findViewById(R.id.name);
            height = view.findViewById(R.id.height);
            description = view.findViewById(R.id.description);
            image = view.findViewById(R.id.imageView);
            delete = view.findViewById(R.id.delete);
        }
    }

    private Drawable getImageDrawableFromId(String imageId){
        int id = this.getContext().getResources().getIdentifier(imageId, "drawable", getContext().getPackageName());
        return this.getContext().getResources().getDrawable(id);
    }
}
