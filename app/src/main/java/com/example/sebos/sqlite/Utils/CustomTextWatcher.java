package com.example.sebos.sqlite.Utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

/**
 * Created by sebos on 27.11.2017.
 */

public class CustomTextWatcher implements TextWatcher {

    private TextView textView;
    private View viewToShow;
    private boolean shouldHideOnEmpty;

    public CustomTextWatcher(TextView textView, View viewToShow, boolean shouldHideOnEmpty){
        this.textView = textView;
        this.viewToShow = viewToShow;
        this.shouldHideOnEmpty = shouldHideOnEmpty;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @SuppressWarnings("ResourceType")
    @Override
    public void afterTextChanged(Editable editable) {
        viewToShow.setVisibility(shouldHideOnEmpty(shouldHideOnEmpty));
    }

    private int shouldHideOnEmpty(boolean shouldHide){
        if (textView.getText().toString().isEmpty()) {
            return shouldHide ? View.GONE : View.VISIBLE;
        } else {
            return shouldHide ? View.VISIBLE : View.GONE;
        }
    }
}

