package com.example.sebos.sqlite;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sebos.sqlite.Utils.CustomArrayAdapter;
import com.example.sebos.sqlite.Utils.DeleteCallback;
import com.example.sebos.sqlite.db.PonyAdapter;
import com.example.sebos.sqlite.db.PonyDao;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements TextWatcher, DeleteCallback {

    @BindView(R.id.ponies)
    ListView ponies;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.height)
    TextView height;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.imageId)
    TextView imageId;
    @BindView(R.id.add)
    FloatingActionButton add;
    @BindView(R.id.emptyList)
    ImageView emptyListImage;

    private PonyDao ponyDao;
    private List<Pony> ponys;
    private CustomArrayAdapter mAdapter;
    private Pony mClickedPony;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        add.setVisibility(View.INVISIBLE);

        name.addTextChangedListener(this);
        height.addTextChangedListener(this);
        description.addTextChangedListener(this);
        imageId.addTextChangedListener(this);

        ponyDao = new PonyAdapter(this);
        ponyDao.open();

        ponyDao.killAll();
        ponyDao.insert(new Pony("Luna", 3.52, "Granatowa", "luna"));
        ponyDao.insert(new Pony("Celestia", 3.22, "Troche biała", "celestia"));
        ponyDao.insert(new Pony("Fluttershy", 1.21, "Żółta", "fluttershy"));
        ponyDao.insert(new Pony("Apple Bloom", 0.72, "Ta tysh", "bloom"));
        ponyDao.insert(new Pony("Rainbow Dash", 1.21, "Tencza <3", "rainbow"));
        ponyDao.insert(new Pony("Twilight Sparkle", 1.21, "Fioletowa", "sparkle"));

        ponys = ponyDao.getPonysHigherThan(0);

        mAdapter = new CustomArrayAdapter(this, ponys, this);
        ponies.setAdapter(mAdapter);

        ponies.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                mClickedPony = (Pony) adapterView.getAdapter().getItem(i);
                name.setText(mClickedPony.getName());
                height.setText(String.valueOf(mClickedPony.getHeight()));
                description.setText(mClickedPony.getDescription());
                imageId.setText(mClickedPony.getImageId());
                return true;
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickedPony == null){
                    insert();
                }else{
                    update();
                }
            }
        });
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //deleteAll();
        Log.i("Count: ", String.valueOf(ponyDao.count()));
        Log.i("isEmpty: ", String.valueOf(ponyDao.isEmpty()));
    }

    private void updateHeight(){
        Toast.makeText(this, String.valueOf(ponyDao.wrapSameHeightToLowest()), Toast.LENGTH_SHORT).show();
        ponys = ponyDao.getPonysHigherThan(0);
        mAdapter.clear();
        mAdapter.addAll(ponys);
        mAdapter.notifyDataSetChanged();
    }

    private void deleteAll(){
        ponyDao.killAll();
        ponys = ponyDao.getPonysHigherThan(0);
        mAdapter.clear();
        mAdapter.addAll(ponys);
        mAdapter.notifyDataSetChanged();
        emptyListImage.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                emptyListImage.setVisibility(View.VISIBLE);
            }
        }).alphaBy(1).setDuration(800).start();

    }

    private void insert(){
        ponyDao.insert(new Pony(name.getText().toString(), Double.valueOf(height.getText().toString()),
                description.getText().toString(), imageId.getText().toString()));
        ponys = ponyDao.getPonysHigherThan(0);
        mAdapter.clear();
        mAdapter.addAll(ponys);
        mAdapter.notifyDataSetChanged();
    }

    private void update(){
        updatePonyWithNewValues();
        Toast.makeText(getApplicationContext(), "Ponies updated: " + ponyDao.updatePony(mClickedPony), Toast.LENGTH_SHORT).show();
        ponys = ponyDao.getPonysHigherThan(0);
        mAdapter.notifyDataSetChanged();
        clearInputs();
        hideKeyboard();
        mClickedPony = null;
    }

    private void hideKeyboard(){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    private void updatePonyWithNewValues(){
        mClickedPony.setName(name.getText().toString());
        mClickedPony.setHeight(Double.valueOf(height.getText().toString()));
        mClickedPony.setDescription(description.getText().toString());
        mClickedPony.setImageId(imageId.getText().toString());
    }

    private void clearInputs(){
        name.setText("");
        height.setText("");
        description.setText("");
        imageId.setText("");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ponyDao.close();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if(isAllFieldsAreEmpty()){
            add.hide();
        }else{
            add.show();
        }
    }

    private boolean isAllFieldsAreEmpty(){
        return name.getText().toString().isEmpty() || height.getText().toString().isEmpty() ||
                description.getText().toString().isEmpty() || imageId.getText().toString().isEmpty();
    }

    @Override
    public void onDeleteItem(int id) {
        Toast.makeText(this, "One pony died", Toast.LENGTH_SHORT).show();
        ponyDao.killOnePony(id);
        ponys = ponyDao.getPonysHigherThan(0);
    }
}
