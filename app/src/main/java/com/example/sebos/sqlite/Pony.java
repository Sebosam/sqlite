package com.example.sebos.sqlite;

/**
 * Created by sebos on 27.11.2017.
 */

public class Pony {

    private int id;
    private String name;
    private double height;
    private String description;
    private String imageId;

    public Pony(String name, double height, String description, String imageId) {
        this.name = name;
        this.height = height;
        this.description = description;
        this.imageId = imageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
